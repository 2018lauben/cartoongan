architecture_dict = {
    "Style Gan": "Style",
    "UNet GAN": "Unet",
    "Fixed GAN": "Fixed",
    "Modular GAN": "Modular",
    "Anime GAN": "Anime",
}

crop_mode_dict = {
    "RESIZE": "Resize",
    "CROP_CENTER": "Center",
    "CROP_RANDOM": "Random",
}

ratio_filter_dict = {
    "NO_FILTER": "None",
    "FILTER_PORTRAIT": "Portrait",
    "FILTER_LANDSCAPE": "Landscape",
}
